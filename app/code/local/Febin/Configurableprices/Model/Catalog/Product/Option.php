<?php
/**
 * Generate custom options for every option and every price tier. This is used to
 * get tier prices for custom options.
 *
 * @category    Febin
 * @package     Febin_Configurableprices
 * @author      Febin Thomas <febinthomas@outlook.com>
 */

class Febin_Configurableprices_Model_Catalog_Product_Option {

	const PREFIX = 'preis-';
	
	/**
	 * Update the product custom options of a configurable product
	 * according to its configurable attribute list.
	 * 
	 * It only deals with the current custom options list of a product that have been commited to the data base. These
	 * are the ones that appear in the product method "getOptions". The custom options that haven't been
	 * commited yet (the ones that appear in the product method "getProductOptions") are ignored.
	 * 
	 * @param array('product' => Mage_Catalog_Model_Product) 
	 */
	public function updateProductCustomOptions($observer) {
		/* @var $product Mage_Catalog_Model_Product */
		$product = $observer['product'];
		
		// We also check if the page with the individual options has been opened at all
		// with !$product->getCanSaveCustomOptions() if not we jump out
		//
		// This is beeing done to prevent errors that show up when not entering the prices 
		if(!$product->isConfigurable() || !$product->getCanSaveCustomOptions()) {
			return;
		}
		
		$newOptionList = $this->_getNewOptionsList($product);		
		foreach($product->getOptions() as $option) {
			if (!strstr($option->getTitle(), self::PREFIX)) {
				continue; // only delete generated options that contain the prefix
			} 
			foreach($option->getValues() as $value) {	
				$key = array_search(strtolower(trim($value->getSku())), $newOptionList);
				if($key === false) {
					$this->_deleteOptionValue($product, $option, $value->getSku());
				} else {
					unset($newOptionList[$key]);
				}	
			}
		}
		foreach($newOptionList as $newOptionValue) {
			$this->addNewOptionValue($product, 0, $newOptionValue);
		}
		
		foreach($product->getOptions() as $option) {
			if (!strstr($option->getTitle(), self::PREFIX)) {
				continue; // only delete generated options that contain the prefix
			}
			if($this->_findProductOption($product, $option->getTitle()) && !$this->_productOptionsHasValue($product, $option)) {
				$this->_deleteOption($product, $option->getTitle());
			} else  if(count($option->getData('values')) > 0) {
				$allOptions = $product->getProductOptions();
				$allOptions[] = $option->getData();
				$product->setProductOptions($allOptions);
			}
		}
	}
	
	/**
	 * Check if the productOption has some value that is not marked to be deleted.
	 * 
	 * @param Mage_Catalog_Model_Product $product
	 * @param Mage_Catalog_Model_Product_Option $option
	 *
	 */
	private function _productOptionsHasValue($product, $option) {
		$optionKey = $this->_findProductOption($product, $option->getTitle());
		if($optionKey !== false){
			$allProductOptions = $product->getProductOptions();
			foreach($allProductOptions[$optionKey]['values'] as $value){
				if($value['is_delete'] != '1'){
					return true;
				}
			}
		}
		return false;
	}
	
	
	/**
	 * 
	 * Delete the options value from the option.
	 * 
	 * @param Mage_Catalog_Model_Product_Option $option
	 * @param String $sku
	 */
	private function _deleteOptionValue($product, $option, $sku) {
		foreach($option->getValues() as $value) {
			if(strcasecmp(trim($value->getSku()), trim($sku)) != 0) {
				continue;
			}
			$allModifiedValuesData = $option->getData('values');
			$value->setData('is_delete','1');
			$allModifiedValuesData[] = $value->getData();
			$option->setData('values',$allModifiedValuesData);

			$allProductOptions = $product->getProductOptions();
			$optionKey = $this->_findProductOption($product, $option->getTitle());
			if ($optionKey === false) {
				$allProductOptions[] = $option->getData();
				$product->setProductOptions($allProductOptions);
				return;
			}
			$valueKey = $this->_findProductOptionValue($allProductOptions[$optionKey],$sku);
			if($valueKey === false) { 
				$allProductOptions[$optionKey]['values'][] =$value->getData();
				$product->setProductOptions($allProductOptions);
				return;
			}
			$allProductOptions[$optionKey]['values'][$valueKey] = $value->getData();
			$product->setProductOptions($allProductOptions);
			return;
		}
	}
	
	/**
	 * Look for the option with $optionTitle in the product Options
	 * 
	 * @param Mage_Catalog_Model_Product $product
	 * @param string $optionTitle
	 */
	private function _findProductOption($product, $optionTitle) {
		$allOptions = $product->getProductOptions();
		foreach($allOptions as $key => $option) {
			if(strcasecmp(trim($option['title']), trim($optionTitle)) == 0) {
				return $key;
			}
		}
		return false;
	}
	
	/**
	 * Look for the option value with $sku in the product option data
	 * 
	 * @param Array $productOptionData 
	 * @param string $sku
	 */
	private function _findProductOptionValue($productOptionData, $sku) {
		foreach($productOptionData['values'] as $key => $option) {
			if(strcasecmp(trim($option['sku']), trim($sku)) == 0) {
				return $key;
			}
		}
		return false;
	}
	
	/**
	 * Add a new option value to the product.If the option does not exists yet, it will be created.
	 * The option which the option value will be added is determined by the sku. 
	 * For example if the sku is "Size-Big-1", the option is the one with "Size" as title. 
	 * 
	 * @param Mage_Catalog_Model_Product $product
	 * @param float $price
	 * @param string $sku
	 */
	public function addNewOptionValue($product, $price, $sku) {
		$newValue =	array('title' => $this->_getOptionValueTitleFromSku($sku), 
						  'price' => $price, 
						  'sku' => $sku,
						  'is_delete' => 0,
						  'price_type' => 'fixed',
						  'option_type_id' => -1);
		
		$optionTitle = $this->_getOptionTitleFromSku($sku);
		foreach($product->getOptions() as $option) {
			if(strcasecmp(trim($option->getTitle()), trim($optionTitle)) == 0) {
				$this->_addValueToOption($option,$newValue);
				return;
			}
		}
		$allOptions = $product->getProductOptions();
        if (!is_array($allOptions)) $allOptions = array();
		foreach($allOptions as $key => $option) {
			if(strcasecmp(trim($option['title']), trim($optionTitle)) == 0) {
				$allOptions[$key]['values'][] = $newValue;
				$product->setProductOptions($allOptions);
				return;
			}
		}
		$this->_addNewOption($product, $optionTitle, array($newValue));
	}
	
	/**
	 * 
	 * Add a new value to the option. If the value already exits, it updates the price.
	 * 
	 * @param Mage_Catalog_Model_Product_Option $option
	 * @param array $newValue
	 */
	private function _addValueToOption($option, $newValue) {
		foreach($option->getValues() as $value) {
			if(strcasecmp(trim($value->getSku()), trim($newValue['sku'])) == 0){
				$value->setData('price',$newValue['price']);
				$value->setStoreId(0); //it is only saved if the storeId is set to 0
				$value->save();
				return;		
			}
		}
		$allValues = $option->getData('values');
		$allValues[] = $newValue;
		$option->setValues($allValues);
	}	
	
	private function _getOptionTitleFromSku($sku){
		$parts = explode('-',$sku);
		return self::PREFIX . $parts[1];		
	}
	
	private function _getOptionValueTitleFromSku($sku){
		$parts = explode('-',$sku);
		$qty = strlen($parts[3]) == 1 ? '0'.$parts[3] : $parts[3]; 
		return self::PREFIX . $parts[2] . '-' . $qty;
	}
	
	/**
	 * If there is no such option, it returns false.
	 * 
	 * @param Mage_Catalog_Model_Product $product
	 */
	private function _getOptionIdByTitle($product, $optionTitle){
		foreach($product->getOptions() as $key => $option){
			if(strcasecmp(trim($option->getTitle()), trim($optionTitle)) == 0){
				return $key;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * Delete product custom option. 
	 * 
	 * @param Mage_Catalog_Model_Product $product
	 * @param String $optionTitle
	 */
	private function _deleteOption($product, $optionTitle) {
		$optionId = $this->_getOptionIdByTitle($product, $optionTitle);
		if(!$optionId){
			return;
		}
		$option = $product->getOptionById($optionId);
		$option->setData('is_delete', 1);
	
		$allOptions = $product->getProductOptions();
		$optionKey = $this->_findProductOption($product, $option->getTitle());
		if ($optionKey !== false) {
			$allOptions[$optionKey] = $option->getData();
		} else {
			$allOptions[] = $option->getData();
		}
		$product->setProductOptions($allOptions);
        $product->setCanSaveCustomOptions(true);
        $product->setHasOption(true);
	}
	
	/**
	 * 
	 * Add a new option to the &product.
	 * 
	 * @param Mage_Catalog_Model_Product $product
	 * @param String $optionTitle - the Options Name
	 * @param array(array()) $values One array of arrays. Each sub-array contains the following keys: 'title','price', 'sku'. 
	 * 							   Exemple: array( array('title' =>'Value Name 1', 'price' => '9000', 'sku' => 'sku1'),
	 * 											   array('title' =>'Value Name 2', 'price' => '9002', 'sku' => 'sku2'));
	 */
	private function _addNewOption($product, $optionTitle, $values) {
		$optionData = array(
		    'is_delete'			=> 0,
        	'previous_group'    => 'select',
        	'title'             => $optionTitle,
        	'type'              => 'drop_down',
        	'is_require'        => 0,
        	'sort_order'        => 0,
        	'price'             => 0,
        	'price_type'        => 'fixed',
		    'values'            => array());
		
		foreach($values as $value) {
			$value['is_delete'] = 0;
			$value['price_type'] = 'fixed';
			$value['option_type_id'] = -1;
			$optionData['values'][] = $value;
		}
		
		$allOptions = $product->getProductOptions();
		$allOptions[] = $optionData;
        $product->setProductOptions($allOptions);
        $product->setCanSaveCustomOptions(true);
        $product->setHasOption(true);
	}
	
	/**
	 * Returns a list of options values sku that the product should have. It
	 * is generated based on the product configurable attributes and associated simple products.
	 * 
	 * The array elements are all lower cased. 
	 * 
	 * @param Mage_Catalog_Model_Product $configurableProduct
	 */
	private function _getNewOptionsList($configurableProduct) {
		$newOptionList = array();
		/* @var $option Mage_Catalog_Model_Product_Option */
		/* @var $value Mage_Catalog_Model_Product_Option_Value */
		foreach ($configurableProduct->getOptions() as $option) {
			if (!$this->_isRelevantOption($option)) {
				continue;
			}
			foreach ($option->getValues() as $value) {
				foreach($configurableProduct->getTierPrice() as $tier){
					$newOptionList[] = strtolower(self::PREFIX . $option->getTitle() . '-' . $value->getTitle() . '-' . $tier['price_qty']);
				}				
			}
		}
		
		return array_unique($newOptionList);
	}
	
	/**
	 * Returns if the option is relevant for generating configurable price options
	 * on top of it. This is currently only true for radio and drop_down options.
	 * 
	 * @param Mage_Catalog_Model_Product_Option $option
	 * @return boolean
	 */
	private function _isRelevantOption($option) {
		return !strstr($option->getTitle(), self::PREFIX) &&
		       ($option->getType() == 'radio' || $option->getType() == 'drop_down');
	}
	
	/**
	 * Returns all saleable simple products associated to the
	 * configurable product.
	 * 
	 * @param Mage_Catalog_Model_Product $product - Configurable product
	 */
	public function getAllowChildrenProducts($product) {
		$products = array();
		$allProducts = $product->getTypeInstance(true)
                ->getUsedProducts(null, $product);
        foreach ($allProducts as $product) {
        	$products[] = $product;
        }
		return $products;
    }
}