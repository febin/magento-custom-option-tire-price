<?php
/**
 * Select options generated in Febin_Configurableprices_Model_Catalog_Product_Option
 * it implement tier prices for custom options. For the current price tier and every
 * option selected there is an option selected.
 *
 * @category    Febin
 * @package     Febin_Configurableprices
 * @author      Febin Thomas <febinthomas@outlook.com>
 */

class Febin_Configurableprices_Model_Checkout_Observer {
    const PREFIX = 'preis-';
    
	/**
	 * 
	 * For the configurable products int the cart, it sets
	 * the product custom options according to the configurable attributes values. 
	 * 
	 * @param $observer
	 */
	public function applyConfigurablePrices($observer) {
		$items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
		$this->_configureItems($items, false);
		
		$cart = $observer['cart'];
		$this->_configureItems($cart->getItems(), true);
	  	$cart->getQuote()->setTotalsCollectedFlag(false);
	  	$cart->getQuote()->getBillingAddress();
        $cart->getQuote()->getShippingAddress()->setCollectShippingRates(true);
        $cart->getQuote()->collectTotals();
        $cart->getCheckoutSession()->setQuoteId($cart->getQuote()->getId());
	}
	
	/**
	 * Set the custom options of $items according to the values of its configurable attributes
	 *  
	 * @param array() $items
	 * @param boolean $saveItem If true it saves the items.
	 * 
	 * TODO: This method has to be split up in muliple methods 
	 */	
	private function _configureItems($items, $saveItem) {
		foreach($items as $item) {
			/* @var $item Mage_Sales_Model_Quote_Item */
			if((strcasecmp(trim($item->getProductType()), "configurable") != 0) || 
			   $item->isDeleted() ||
			   !$item->getProduct()->getTierPrice()) { 
				continue; 
			}
			$itemQty  = $this->_getQuantityBoundary($item);
            $options_ids = $this->_getNotGeneratedCustomOptions($item);
			$optionIdArray = array_filter(explode(',', $options_ids));
            
            foreach ($optionIdArray as $optionId) {
            	$valueId = $item->getOptionByCode('option_' . $optionId)->getValue();
            	$productOption = $item->getProduct()->getOptionById($optionId);
            	
            	if (!($productOption->getType() == 'radio' || $productOption->getType() == 'drop_down')) {
            		continue; // We do only support typs with one selection
            	}
            	
            	$optionTitle = $productOption->getTitle();
            	$valueTitle = $productOption->getValueById($valueId)->getTitle();
            	$option =  $this->_getOptionByTitle($item->getProduct(), self::PREFIX . $optionTitle);
            	
            	$valueSku =  self::PREFIX . $optionTitle . "-" . $valueTitle . "-" . $itemQty;
            	$this->_addOrUpdateOptionValue($item, 'option_' . $option->getId(), $this->_getOptionValueIdBySku($option,$valueSku));
            	$options_ids .= strlen($options_ids) == 0 ? $option->getId() : "," . $option->getId();
            }
            
            $this->_addOrUpdateOptionValue($item, 'option_ids', $options_ids);

            if ($saveItem) {
            	$item->save();
            }
	  	}
	}

	/**
	 * Returns the custom_options that have not been generated.
	 * 
	 * @param Mage_Sales_Model_Quote_Item $item
	 */
	private function _getNotGeneratedCustomOptions($item) {
		$option_ids = "";
		foreach ($item->getProduct()->getOptions() as $option) {
			if (!strstr($option->getTitle(), self::PREFIX) &&
				$item->getOptionByCode('option_' . $option->getOptionId())) {
				$option_ids .= strlen($option_ids) == 0 ? $option->getOptionId() : "," . $option->getOptionId();
			}
		}
		return $option_ids;
	}
	
	/**
	 * If $item does not contains a option with the $optionCode, it adds a new option
	 * with the value $optionValueId. Othewise it updates the option value.
	 * 
	 * @param Mage_Sales_Model_Quote_Item $item
	 * @param String $optionCode
	 * @param String $optionValueId
	 */
	private function _addOrUpdateOptionValue($item, $optionCode, $optionValueId) {
		if (!$item->getOptionByCode($optionCode)) {
            $item->addOption(new Varien_Object(
            			array( 'product' => $item->getProduct(),
                           'code' => $optionCode,
                           'value' => $optionValueId )));
		} else {
            $optionTmp = $item->getOptionByCode($optionCode);
            $optionTmp->setValue($optionValueId);
		}
	}
	
	/**
	 * Calculate which boundary the given item->getQty() belongs to.
	 * 
	 * @param Mage_Sales_Model_Quote_Item $item
	 * @return int
	 */
	private function _getQuantityBoundary($item) {
		$boundary = 0;
		foreach($item->getProduct()->getTierPrice() as $tier){
			if($item->getQty() >= $tier['price_qty']) {
				$boundary = (int) $tier['price_qty'];
			}			
		}
		return $boundary;
	}
	
	/**
	 * 
	 * Returns the ID of one Option Value given its SKU.
	 * the method is case-insensitive.
	 * 
	 * @param String $valueSku
	 * @param Mage_Catalog_Model_Product_Option $option
	 * 
	 * @return int Option Value ID
	 */
	private function _getOptionValueIdBySku($option, $valueSku) {
		foreach ($option->getValuesCollection() as $value) {
	    	if (strcasecmp(trim($value->getSku()),trim($valueSku)) == 0) {
	    		return $value->getOptionTypeId();
	    	}
	    }
	}
	
	/**
	 * 
	 * Looks for the product option with the same name (title) of the $optionName param.
	 * this method is case-insensitve.
	 * 
	 * @param String $optionName
	 * @param Mage_Catalog_Model_Product $product
	 * 
	 * @return Mage_Catalog_Model_Product_Option|null
	 */
	private function _getOptionByTitle($product, $optionName) {
		foreach ($product->getOptions() as $option) {
			if (strcasecmp(trim($option->getTitle()), trim($optionName)) == 0 ) {
				return $option;
			}
		}
 	}

}