<?php
/**
 * Frontend Model for Weight conditions
 *
 * @category    Febin
 * @package     Febin_Configurableprices
 * @author      Febin Thomas <febinthomas@outlook.com>
 */

class Febin_Configurableprices_Block_Product_View_Options extends Mage_Catalog_Block_Product_View_Options {
	const PREFIX = 'preis-';
	
	/**
	 * Return nothing if the option is generated for the configurable prices
	 *
	 * @param Mage_Catalog_Model_Product_Option $option
	 */
	public function getOptionHtml(Mage_Catalog_Model_Product_Option $option) {
		if (strstr($option->getTitle(), self::PREFIX)) {
			return '';
		} else {
			return parent::getOptionHtml($option);
		}
	}
}
