<?php
/**
 * Frontend Model for Weight conditions
 *
 * @category    Febin
 * @package     Febin_Configurableprices
 * @author      Febin Thomas <febinthomas@outlook.com>
 */

class Febin_Configurableprices_Block_Cart_Item_Renderer_Configurable extends Mage_Checkout_Block_Cart_Item_Renderer_Configurable {
	const PREFIX = 'preis-';
	
	/**
	 * Just return the non-generated options
	 *
     * @return array
     */
    public function getOptionList() {
    	$options = array();
    	
    	foreach (parent::getOptionList() as $option) {
    		if (!strstr($option['label'], self::PREFIX)) {
    			$options[] = $option;
    		}
    	}

        return $options;
    }
}
